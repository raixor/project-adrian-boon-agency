const TokenManagement = require("./utils/tokenmanagement");

module.exports = class Response {
    static Success(data) {
        return {
            error: false,
            errorMessage: "",
            token: TokenManagement.token,
            result: data
        };
    }

    static Error(data) {
        return {
            error: true,
            errorMessage: data,
            token: "",
            result: ""
        };
    }

}