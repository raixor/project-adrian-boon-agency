const express = require('express');
const Event = require("./../models/event.model");
const UserAttend = require("./../models/user_attend.model");
let router = express.Router();
let Response = require("./../responseObj.js");
const TokenManagement = require("./../utils/tokenmanagement");
const passport = require('passport');
const User = require("./../models/user.model");
const ImageUtils = require("./../utils/ImageUtils");

router.use((req, res, next) => {
    passport.authenticate('jwt', function (err, user, info) {
        if (err || !user) { return res.send(Response.Error("User not autenticated")); }
        else next();
    })(req, res, next);
});

router.get('/:offset?', (req, res) => {
    let queryParams = {
        include: [{ all: true }],
        limit: 8
    };

    if (req.params.offset)
        queryParams.offset = Number(req.params.offset);

    Event.findAll(queryParams).then(events => {
        let result = events.map(event => {
            event.mine = (TokenManagement.currentUser.id === event.creator)
            return event;
        });
        res.json(Response.Success(events))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});

router.get('/attend/:id', (req, res) => {
    UserAttend.findAll({
        attributes: ['user'],
        where: {
            event: req.params.id
        },
        include: [{ model: User, as: 'userData' }]
    }).then(events => {
        events.map(event => {
            events.mine = (TokenManagement.currentUser.id === event.creator)
        });
        res.json(Response.Success(events))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});

router.post('/attend/:id', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {

        Event.findAndCountAll({
            where: {
                id: req.params.id
            },
            limit: 1
        }).then(result => {
            if (result.count > 0) {
                UserAttend.create({ user: tok.data.id, event: req.params.id, tickets: req.body.quantity }).then(events => {
                    res.json(Response.Success())
                }).catch(error => {
                    res.json(Response.Error(error.parent.sqlMessage));
                });
            } else {
                res.send(Response.Error("Event not found"));
            }
        }).catch(error => {
            res.json(Response.Error(error));
        })

    });
});

router.delete('/attend/:id', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {
        UserAttend.destroy({
            where: {
                event: req.params.id,
                user: tok.data.id
            }
        }).then(event => {
            if (event)
                res.json(Response.Success())
            else
                res.json(Response.Error("Event not found."))
        }).catch(error => {
            res.json(Response.Error(error.parent.code));

        });
    });
});

router.get('/mine', (req, res) => {
    UserAttend.findAll({
        attributes: ['event'],
        where: {
            user: TokenManagement.currentUser.id
        },
        include: [{ model: Event, as: 'eventData' }]
    }).then(events => {
        events.map(event => {
            events.mine = (TokenManagement.currentUser.id === event.creator)
        });
        res.json(Response.Success(events))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});

router.get('/created/:id', (req, res) => {
    Event.findAll({
        where: {
            creator: req.params.id
        },
        include: [{ all: true }],
    }).then(events => {
        events.map(event => {
            events.mine = (TokenManagement.currentUser.id === event.creator)
        });
        res.json(Response.Success(events))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});

router.get('/info/:id', (req, res) => {
    Event.findOne({
        where: {
            id: req.params.id
        },
        include: [{ all: true }]
    }).then(events => {
        events.mine = (TokenManagement.currentUser.id === events.creator)
        res.json(Response.Success(events))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});

router.post('/', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {
        req.body.creator = tok.data.id;
        Event.create(req.body).then(event => {
            ImageUtils.save(req.body.image, event.id, 'public/images/events/' + event.id);
            event.update({ image: event.id + "/" + event.id + '.' + ImageUtils.getMimeType(req.body.image) });
            event.img = event.id + "/" + event.id + '.' + ImageUtils.getMimeType(req.body.image);
            res.json(Response.Success(event))
        }).catch(error => {
            res.json(Response.Error(error.parent));

        });
    });
});

router.put('/:id', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {
        req.body.creator = tok.data.id;
        Event.findOne({
            where: {
                id: req.params.id
            }
        }).then(event => {
            ImageUtils.delete('public/images/events/' + event.image);
            ImageUtils.save(req.body.image, event.id, 'public/images/events/' + event.id);
            event.img = event.id + "/" + event.id + '.' + ImageUtils.getMimeType(req.body.image);
            req.body.image = event.img;
            event.update(req.body);
            res.json(Response.Success(event))
        }).catch(error => {
            res.json(Response.Error(error.parent));

        });
    });
});

router.delete('/:id', (req, res) => {
    Event.destroy({
        where: {
            id: req.params.id
        }
    }).then(event => {
        if (event) {
            ImageUtils.delete('public/images/events/' + event.image);
            res.json(Response.Success())
        } else
            res.json(Response.Error("Event not found."))
    }).catch(error => {
        res.json(Response.Error(error.parent.code));

    });

});


module.exports = router;