const express = require('express');
let router = express.Router();
const Event = require("./../models/event.model");
const UserAttend = require("./../models/user_attend.model");
let Response = require("./../responseObj.js");
const TokenManagement = require("./../utils/tokenmanagement");
const passport = require('passport');
const User = require("./../models/user.model");
const ImageUtils = require("./../utils/ImageUtils");
const bcrypt = require('bcrypt-nodejs');

router.use((req, res, next) => {
    passport.authenticate('jwt', function (err, user, info) {
        if (err || !user) { return res.send(Response.Error("User not autenticated")); }
        else next();
    })(req, res, next);
});

router.get('/', (req, res) => {
});


router.get('/attend/:id', (req, res) => {
    UserAttend.findAll({
        attributes: ['event'],
        where: {
            user: req.params.id
        },
        include: [{ model: Event, as: 'eventData' }]
    }).then(events => {
        events.map(event => {
            events.mine = (TokenManagement.currentUser.id === event.creator)
        });
        res.json(Response.Success(events))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});

router.get('/me', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {
        User.findAll({
            where: {
                id: tok.data.id
            }
        }).then(user => {

            res.json(Response.Success(user))
        }).catch(error => {
            res.json(Response.Error(error.errors));

        });
    });
});

router.get('/:id', (req, res) => {
    User.findOne({
        where: {
            id: req.params.id
        }
    }).then(user => {
        if (user)
            res.json(Response.Success(user))
        else
            res.json(Response.Error("User not found"))
    }).catch(error => {
        res.json(Response.Error(error.errors));

    });
});


router.put('/me', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {
        User.findOne({
            where: {
                id: tok.data.id
            }
        }).then(user => {
            if (user) {
                ImageUtils.delete('public/images/events/' + user.image);
                ImageUtils.save(req.body.image, user.id, 'public/images/users/' + user.id);
                req.body.image = user.id + "/" + user.id + '.' + ImageUtils.getMimeType(req.body.image);
                // if(req.body.password== ''){
                //     delete req.body.password;
                // }
                user.update(req.body).then(() => {
                    res.json(Response.Success());
                }).catch(err => {
                    res.json(Response.Error(error));
                });
            } else
                res.json(Response.Error("User not found"))

        }).catch(error => {
            res.json(Response.Error(error.errors));

        });
    });
});

router.put('/:id', (req, res) => {
    TokenManagement.verifyToken(req.get("Authorization")).then(tok => {
        User.findOne({
            where: {
                id: req.params.id
            }
        }).then(user => {
            if (user) {
                if(user.image!='')
                    ImageUtils.delete('public/images/users/' + user.image);
                    console.log(req.body);
                if (!req.body.password || req.body.password == '') {
                    delete req.body.password;
                    console.log(req.body);
                    ImageUtils.save(req.body.image, user.id, 'public/images/users/' + user.id);
                    req.body.image = user.id + "/" + user.id + '.' + ImageUtils.getMimeType(req.body.image);
                    user.update(req.body).then(() => {
                        res.json(Response.Success(req.body));
                    }).catch(err => {
                        res.json(Response.Error(error));
                    });
                } else {
                    bcrypt.hash(req.body.password, null, null, function (err, hash) {
                        ImageUtils.save(req.body.image, user.id, 'public/images/users/' + user.id);
                        req.body.image = user.id + "/" + user.id + '.' + ImageUtils.getMimeType(req.body.image);
                        req.body.password = hash;
                        user.update(req.body).then(() => {
                            res.json(Response.Success(req.body));
                        }).catch(err => {
                            res.json(Response.Error(error));
                        });
                    });
                }
            } else
                res.json(Response.Error("User not found"))
        }).catch(error => {
            res.json(Response.Error(error));

        });
    });
});

module.exports = router;