const express = require('express');
let router = express.Router();
const User = require('./../models/user.model');
let Response = require("./../responseObj.js");
const bcrypt = require('bcrypt-nodejs');
const TokenManagement = require("./../utils/tokenmanagement");
const ImageUtils = require("./../utils/ImageUtils");
const http = require('http');
const https = require('https');
const base64 = require('base64-img');

router.post('/login', (req, res) => {
    if (req.body) {
        User.findOne({ where: { email: req.body.email } }).then(userResult => {
            if (userResult) {
                bcrypt.compare(req.body.password, userResult.dataValues.password, (error, result) => {
                    if (result && result.password != "") {
                        TokenManagement.generateToken(userResult.dataValues);
                        userResult.update({ lat: req.body.lat, lng: req.body.lng });
                        res.json(Response.Success(userResult.dataValues))
                    } else if (userResult.password == "") {
                        res.json(Response.Error("Account only available with google login or facebook."))
                    } else {
                        res.json(Response.Error("Incorrect password."));
                    }
                });
            } else {
                res.json(Response.Error("User not found"))
            }

        }).catch(error => {
            res.json(Response.Error("User or password incorrect"));
        });

    } else {
        res.json(Response.Error("Bad request"))
    }
});

router.post('/register', (req, res) => {
    bcrypt.hash(req.body.password, null, null, function (err, hash) {
        req.body.password = hash;
        User.create(req.body).then(user => {
            if (req.body.image) {
                ImageUtils.save(req.body.image, user.id, 'public/images/users/' + user.id);
                user.update({ image: user.id + "/" + user.id + '.' + ImageUtils.getMimeType(req.body.image) });
            }
            res.json(Response.Success(user))
        }).catch(error => {
            res.json(Response.Error(error.errors));

        });
    });
});

router.get('/token', (req, res) => {
    let token = req.get("authorization");
    TokenManagement.verifyToken(token).then(tkn => {
        res.json(Response.Success(tkn.data));
    }).catch(error => {
        res.json(Response.Error(error));
    });
});

router.post('/google', (req, res) => {
    https.request('https://www.googleapis.com/plus/v1/people/me?access_token=' + req.body.accessToken)
        .on('response', function (responseGoogle) {
            getBody(responseGoogle).then(body => {
                User.findOrCreate({
                    where: { email: body.emails[0].value },
                    defaults: {
                        name: body.displayName,
                        email: body.emails[0].value,
                        lat: req.body.lat,
                        lng: req.body.lng,
                        id_Google: body.id
                    }
                }).spread((user, created) => {
                    TokenManagement.generateToken(user.dataValues);
                    if (created) {
                        base64.requestBase64(body.image.url, function (err, response, bodyImg) {
                            if (!err) {
                                ImageUtils.save(bodyImg, user.id, 'public/images/users/' + user.id);
                                user.update({ image: user.id + "/" + user.id + '.' + ImageUtils.getMimeType(bodyImg) });
                            }

                            res.send(Response.Success(user.dataValues));
                        });
                    } else {
                        res.send(Response.Success(user.dataValues));
                    }
                });
            }).catch(err => {
                res.send(Response.Error(err));
            })
        }).end();
});

router.post('/facebook', (req, res) => {
    https.request('https://graph.facebook.com/v2.11/me?fields=id,name,email,picture&access_token=' + req.body.facebookData.authResponse.accessToken)
        .on('response', function (responseFacebook) {
            getBody(responseFacebook).then(bodyResolve => {
                User.findOrCreate({
                    where: { email: bodyResolve.email },
                    defaults: {
                        name: bodyResolve.name,
                        email: bodyResolve.email,
                        lat: req.body.lat,
                        lng: req.body.lng,
                        id_Facebook: bodyResolve.id
                    }
                }).spread((user, created) => {
                    TokenManagement.generateToken(user.dataValues);
                    if (created) {
                        base64.requestBase64(bodyResolve.picture.data.url, function (err, response, bodyImg) {
                            if (!err) {
                                ImageUtils.save(bodyImg, user.id, 'public/images/users/' + user.id);
                                user.update({ image: user.id + "/" + user.id + '.' + ImageUtils.getMimeType(bodyImg) });
                            }

                            res.send(Response.Success(user.dataValues));
                        });
                    } else {
                        res.send(Response.Success(user.dataValues));
                    }
                });

            }).catch(error => {
                res.send(Response.Error("Failed to get the data. Detail: " + err));
            });
        })
        .end();
});

/**
 * Para las peticiones PUT|POST|DELETE devuelve el cuerpo de la petición.
 * @param {*} request 
 */
function getBody(request) {
    let body = [];
    return new Promise((resolve, reject) => {
        request.on('error', (err) => {
            reject(err);
        }).on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            resolve(JSON.parse(Buffer.concat(body).toString()));
        });
    });
}

module.exports = router;