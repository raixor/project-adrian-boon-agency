const sequelize = require('./../config.db');
const Sequelize = require('sequelize');
const User = require("./user.model");
const Event = sequelize.define('event', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    creator: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id',
        },
        notNull: true,
        notEmpty: true,
    },
    title: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
    },
    description: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
    },
    date: {
        type: Sequelize.DATE,
        notNull: true,
        notEmpty: true,
    },
    price: {
        type: Sequelize.INTEGER,
        notNull: true,
        notEmpty: true,
    },
    lat: {
        type: Sequelize.DECIMAL,
        notNull: true,
        notEmpty: true,
    },
    lng: {
        type: Sequelize.DECIMAL,
        notNull: true,
        notEmpty: true,
    },
    address: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
    },
    image: Sequelize.STRING,
    numAttend: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    }
}, {
    tableName: 'event',
    createdAt: false,
    updatedAt: false,
});
Event.belongsTo(User, {foreignKey: 'creator', as: 'creatorData'});

module.exports = Event;