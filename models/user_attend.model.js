const sequelize = require('./../config.db');
const Sequelize = require('sequelize');
const User = require("./user.model");
const Event = require("./event.model");

const UserAttend = sequelize.define('user_attend_event', {
    user: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id',
        },
        primaryKey: true,
        notNull: true,
        notEmpty: true,
    },
    event: {
        type: Sequelize.INTEGER,
        references: {
            model: Event,
            key: 'id',
        },
        primaryKey: true,
        notNull: true,
        notEmpty: true,
    },
    tickets: {
        type: Sequelize.INTEGER,
        notNull: true,
        notEmpty: true,
    }
}, {
    tableName: 'user_attend_event',
    createdAt: false,
    updatedAt: false,
});
UserAttend.belongsTo(User, {foreignKey: 'user', as: 'userData'});
UserAttend.belongsTo(Event, {foreignKey: 'event', as: 'eventData'});
// UserAttend.belongsTo(Event, {foreignKey: 'event', as: 'creatorData'});

module.exports = UserAttend;