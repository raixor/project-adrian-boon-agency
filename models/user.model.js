const sequelize = require('./../config.db');
const Sequelize = require('sequelize');

const User = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        notNull: true,
        notEmpty: true,
    },
    password: {
        type: Sequelize.STRING,
        notNull: true,
    },
    image: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
    },
    lat: {
        type: Sequelize.DECIMAL,
        notNull: true,
        notEmpty: true,
    },
    lng: {
        type: Sequelize.DECIMAL,
        notNull: true,
        notEmpty: true,
    },
    id_Facebook: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null,
    },
    id_Google: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: null,
    }
}, {
        tableName: 'user',
        createdAt: false,
        updatedAt: false,
    });

module.exports = User;