
const fs = require('fs');
const Path = require('path')
module.exports = class ImageUtils {

    /**
     * Guarda en una ruta especifica una imagen en formto base64.
     * @param {string} data Imagen en base64
     * @param {string} name Nombre de la imagen SIN EXTENSION.
     * @param {string} path Ruta relativa donde se va a guardar la imagen.
     */
    static save(data, name, path) {
        if (!fs.existsSync(path))
            fs.mkdirSync(path);
        let extension = ImageUtils.getMimeType(data);

        data = data.replace(/^data:image\/\w+;base64,/, "");
        let archivo = { bytes: data, nombreArchivo: (path + '/' + name + '.' + extension) };
        let datos = Buffer.from(archivo.bytes, 'base64');
        fs.writeFileSync(archivo.nombreArchivo, datos);
    }

    /**
     * Borra un archivo.
     * Si este archivo se encuentra en un directorio y este directorio se queda vacio al 
     *  borrar el archivo el directorio tambien se borrara.
     * @param {string} path 
     */
    static delete(path) {
        if(!fs.existsSync(path))
            return;
        
        fs.unlinkSync(path);
        let files = fs.readdirSync(Path.dirname(path));
        if(files.length <= 0)
            fs.rmdirSync(Path.dirname(path));
    }

    /**
     * Devuelve la extension del archivo en base64.
     * @param {string} encoded Imagen en base64
     */
    static getMimeType(encoded) {
        var result = null;

        if (typeof encoded !== 'string') {
            return result;
        }

        var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
      

        if (mime && mime.length) {
            mime = mime[1].split('/');
            return mime[1];
        }

        return result;
    }
}