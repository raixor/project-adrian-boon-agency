const jwt = require('jsonwebtoken');
const SECRETO = "ADRIANSANCHISGALLEGO";
const { Strategy, ExtractJwt } = require('passport-jwt');

module.exports = class TokenManagement {
    static get token() {
        return this._token;
    }
    static set token(value) {
        this._token = value;
    }

    static get currentUser() {
        return this._currentUser;
    }
    static set currentUser(value) {
        this._currentUser = value;
    }

    static generateToken(data) {
        this.token = jwt.sign({ data: data }, SECRETO, { expiresIn: "2 hours" });
        this.currentUser = data;
        return this.token;
    }

    static getStrategy() {

        return new Strategy({
            secretOrKey: SECRETO, jwtFromRequest:
                ExtractJwt.fromAuthHeaderAsBearerToken()
        }, (payload, done) => {
            if (payload.data) {
                return done(null, { data: payload.data });
            } else {
                return done({error: "User not autenticated"}, null);
            }
        })
    }

    static verifyToken(token) {
        return new Promise((resolve, reject) => {
            token = token.replace(/^Bearer\s/, '');
            try {
                let resultado = jwt.verify(token, SECRETO, function (err, token) {
                    if (err) {
                        reject("User not autenticated. Detail" + err);
                    } else {
                        // this.currentUser = token.data;
                        resolve(token);
                    }
                });

            }
            catch (e) { reject("User not autenticated. Detail " + e); }
            reject("User not autenticated");
        });
    }

    static renovarToken(token) {
        token = token.replace(/^Bearer\s/, '');
        TokenManagement.verifyToken(token).then(tok => {
            this.currentUser = tok.data;
            this.token = TokenManagement.generateToken(tok.data);
        });
    }

}