const express = require('express');
const bodyParser = require('body-parser')
const passport = require('passport');

const auth = require('./routes/auth');
const users = require('./routes/users');
const events = require('./routes/events');
const main = require('./routes/main');
const Sequelize = require('sequelize');
const TokenManagement = require("./utils/tokenmanagement");
const jwt = require('jsonwebtoken');
let Response = require("./responseObj.js");
const cors = require('cors')


let app = express();
app.use(passport.initialize());
app.use(bodyParser.json({limit: '150mb'})); 
app.use(cors())
passport.use(TokenManagement.getStrategy());

passport.use(TokenManagement.getStrategy(), (payload, done) => {
    (payload);
    Usuario.findOne({ _id: payload.data._id })
        .then(resultado => {
            if (resultado)
                return done(null, { data: payload });
            else
                return done(Response.Error("User not autenticated."), null);
        });
});

app.use((req, res, next) => {
    (req.url);
    if (req.get("authorization")) {
        TokenManagement.renovarToken(req.get("authorization"));
    }
    next();

});
app.use('/public', express.static('./public/'));

app.use('/auth', auth);
app.use('/users', users);
app.use('/events', events);

app.use('/', main);

app.listen(8585);
